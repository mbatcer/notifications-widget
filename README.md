# notification-widget

A simple notification widget 

## Usage

```tsx
import * as React from 'react'

import NotificationsWidget from 'notification-widget'

class Example extends React.Component {
  render () {
    return (
      <NotificationsWidget/>
    )
  }
}
```

then call it this way: `NotificationsWidget.show(message, position, type)`, where 

`message` (string): Message to show the user

`position` (string): One of four corners of the screen ('tl', 'tr', 'bl', 'br')

`type` (string): type of notifications: 'alert', 'info' or 'warning', each of them has its color (red, blue or yellow)

