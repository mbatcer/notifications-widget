import NotificationsWidget from './'

describe('NotificationsWidget', () => {
  it('has show method which accepts 3 arguments', () => {
    const widget = new NotificationsWidget({});
    expect(widget).toHaveProperty('show');
    expect(typeof widget.show).toEqual('function');
    expect(widget.show.length).toBe(3)
  })
});
