/**
 * @class NotificationsWidget
 */

import * as React from 'react'

import './styles.css'

export type Props = {}

export type State = { items: Notification[] }

export type Notification = { message: string, type: string, position: string }

class NotificationsWidget extends React.Component<Props, State> {

  HIDE_TIMEOUT = 5000;

  OFFSET_INCREMENT = 12;

  TYPES = ['info', 'warning', 'alert'];

  DEFAULT_TYPE = this.TYPES[0];

  POSITIONS = ['tl', 'tr', 'bl', 'br'];

  DEFAULT_POSITION = this.POSITIONS[0];

  state = {items: []};

  componentDidMount() {
    window['NotificationsWidget'] = this;
  };

  show = (message: string, type: string, position: string) => {
    if (!this.TYPES.includes(type)) {
      type = this.DEFAULT_TYPE;
    }
    if (!this.POSITIONS.includes(position)) {
      position = this.DEFAULT_POSITION;
    }
    this.addItem({message, type, position});
    setTimeout(this.removeItem, this.HIDE_TIMEOUT)
  };

  addItem = (item: Notification) => {
    this.setState(prevState => ({items: [...prevState.items, item]}))
  };

  removeItem = () => {
    this.setState(prevState => ({items: prevState.items.slice(1)}))
  };

  render() {
    return this.POSITIONS.map(
      (position: string) => Stack(this.state.items, position, this.OFFSET_INCREMENT)
    )
  }
}

const Stack = (items: Notification[], position: string, offsetIncrement: number) => {
  const stackItems = (items: Notification[], position: string) =>
    items.filter((item: Notification) => item.position === position);

  const positionStyle = (position: string, index: number) => {
    const stackingOffset = index * offsetIncrement;
    return {
      ...(position[0] === 't' ? {top: `${stackingOffset}px`} : {bottom: `${stackingOffset}px`}),
      ...(position[1] === 'l' ? {left: 0} : {right: 0})
    }
  };

  return stackItems(items, position).map((item: Notification, index: number) => (
    <div key={index}
         style={positionStyle(item.position, index)}
         className={`notificationWidget
                     notificationWidget--type--${item.type}`}>
      {item.message}
    </div>
  ))
};

export default NotificationsWidget;
